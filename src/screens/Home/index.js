import React, { useState, useEffect } from "react";
import axios from "axios";
import { SafeAreaView, FlatList } from "react-native";
import PostItem from "../../components/PostItem";

export default function Home({ navigation }) {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getPosts();
  }, []);

  async function getPosts() {
    setLoading(true);

    const response = await axios.get(`http://192.168.1.102:8000/api/posts`);
    const posts = response.data.posts;

    setPosts(posts);
    setLoading(false);
  }

  return (
    <SafeAreaView>
      <FlatList
        data={posts}
        renderItem={({ item }) => (
          <PostItem item={item} navigation={navigation} />
        )}
        keyExtractor={(item) => String(item.id)}
        refreshing={loading}
        onRefresh={getPosts}
      />
    </SafeAreaView>
  );
}
