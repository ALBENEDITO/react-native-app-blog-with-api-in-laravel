import React, { useState, useEffect } from "react";
import axios from "axios";
import { ScrollView, Text, StyleSheet, View } from "react-native";

import PostItem from "./../../components/PostItem";

export default function Single({ route, navigation }) {
  const [post, setPost] = useState(null);
  const { id } = route.params;

  useEffect(() => {
    getPost();
  }, []);

  async function getPost() {
    const response = await axios.get(
      `http://192.168.1.102:8000/api/posts/show/${id}`
    );

    setPost(response.data.post);
  }

  return (
    <ScrollView>
      <View style={styles.container}>
        {post && (
          <>
            <Text style={{ marginLeft: 10 }}>{post.title}</Text>
            <PostItem item={post} navigation={navigation} />
          </>
        )}
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: 15,
  },
});
