import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { AntDesign } from "@expo/vector-icons";

import Home from "../screens/Home";
import Single from "../screens/Single";

const Stack = createStackNavigator();

export default function Navigation() {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Home"
        component={Home}
        options={{
          title: "Blog",
          headerLeft: () => (
            <AntDesign
              style={{ marginLeft: 10 }}
              name="home"
              size={24}
              color="black"
            />
          ),
        }}
      />
      <Stack.Screen
        name="Single"
        component={Single}
        options={{ title: "Post View" }}
      />
    </Stack.Navigator>
  );
}
